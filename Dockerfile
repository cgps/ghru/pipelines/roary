FROM continuumio/miniconda3
LABEL authors="Anthony Underwood" \
      description="Docker image containing all requirements for roary pipeline"

RUN apt update; apt install -y gcc procps libidn11

COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
# update to latest copy of tbl2asn
RUN wget ftp://ftp.ncbi.nih.gov/toolbox/ncbi_tools/converters/by_program/tbl2asn/linux64.tbl2asn.gz -O linux64.tbl2asn.gz 
RUN gunzip linux64.tbl2asn.gz && mv linux64.tbl2asn /opt/conda/envs/ghru-roary/bin/tbl2asn && chmod 755 /opt/conda/envs/ghru-roary/bin/tbl2asn

ENV PATH /opt/conda/envs/ghru-roary/bin:$PATH
