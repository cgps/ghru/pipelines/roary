class Utility {
    static def find_genome_size(sample_id, mash_output){
        def match = mash_output =~ /Estimated genome size: (.+)/
        def genome_size = Float.parseFloat(match[0][1]).toInteger()
        return [sample_id, genome_size]
    }

    static def find_total_number_of_reads(sample_id, seqtk_fqchk_ouput){
        def match = seqtk_fqchk_ouput =~ /ALL\s+(\d+)\s/
        def total_reads = match[0][1].toInteger() * 2 // the *2 is an estimate since number of reads >q25 in R2 may not be the same
        return [sample_id, total_reads]
    }
}
