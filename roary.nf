#!/usr/bin/env nextflow
/*

========================================================================================
                          GHRU Roary Pipeline
========================================================================================
 Prokka for Annotation
 Roary for Pangenome analysis
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.1.3'

def versionMessage(){
  log.info"""
  ==============================================
   Roary Pipeline version ${version}
  ==============================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Mandatory arguments:
      --input_dir             Path to input dir containing assembly fasta files. This must be used in conjunction with fasta_pattern
      --fasta_pattern         The regular expression that will match fasta files e.g '*_scaffolds.fas'
      --output_dir            Path to output dir
    Optional arguments:
      --blastp_cutoff         minimum percentage identity for blastp, default 95
      --max_clusters          maximum number of clusters, default 50000
      --num_cores_for_roary   number of cores to use in the roary process
      --tree                  Generate a tree from the core genome alignment using iqtree                             
   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}
/***************** Setup inputs and channels ************************/
params.input_dir = false
params.fasta_pattern = false
params.output_dir = false
params.tree = false
params.blastp_cutoff = false
params.max_clusters = false
params.num_cores_for_roary = false

if (params.blastp_cutoff){
  blastp_cutoff = params.blastp_cutoff
} else {
  blastp_cutoff = 95
}

if (params.max_clusters){
  max_clusters = params.max_clusters
} else {
  max_clusters = 50000
}

if (params.num_cores_for_roary){
  num_cores_for_roary = params.num_cores_for_roary
} else {
  num_cores_for_roary = 4
}


// set up input and output paths
input_dir = Helper.check_mandatory_parameter(params, 'input_dir') - ~/\/$/
fasta_pattern = Helper.check_mandatory_parameter(params, 'fasta_pattern')
output_dir = Helper.check_mandatory_parameter(params, 'output_dir') - ~/\/$/


log.info "======================================================================"
log.info "                    Roary pipeline"
log.info "======================================================================"
log.info "Pipeline version   : ${version}"
log.info "Fasta inputs      : ${input_dir}/${fasta_pattern}"
log.info "======================================================================"
log.info "Outputs written to path     : ${params.output_dir}"
log.info "======================================================================"
log.info ""



fasta_filepath = input_dir + '/' + fasta_pattern
Channel
  .fromPath( fasta_filepath )
  .ifEmpty { error "Cannot find any files matching: ${fasta_filepath}" }
  .map{ file -> tuple(Channel.readPrefix(file, fasta_pattern), file) }
  .set { assemblies }

process prokka {
    tag "${id}"
    publishDir "${output_dir}/prokka",
        mode: "copy",
        saveAs: { file -> file.split('\\/')[-1] }
    
    input:
    set id, file(assembly) from assemblies

    output:
    file("${id}/*.gff") into gff_files

    script:
    """
    prokka --cpus 1 --prefix ${id} ${assembly} 
    """
}

process roary {
    tag "roary"
    publishDir "${output_dir}/roary", mode: "copy"

    cpus num_cores_for_roary
    memory  { 4.GB * num_cores_for_roary }

    input:
    file(input) from gff_files.collect { it }

    output:
    file('accessory_binary_genes.fa.newick')
    file('accessory_graph.dot')
    file('core_accessory_graph.dot')
    file('gene_presence_absence.csv')
    file('gene_presence_absence.Rtab')
    file('summary_statistics.txt')
    file('number_of_unique_genes.Rtab')
    file('number_of_new_genes.Rtab')
    file('number_of_genes_in_pan_genome.Rtab')
    file('number_of_conserved_genes.Rtab')
    file('core_accessory.tab')
    file('core_accessory.header.embl')
    file('accessory.tab')
    file('accessory.header.embl')
    file('pan_genome_reference.fa')
    file('pan_genome_sequences')
    file('core_gene_alignment.aln') into core_gene_alignment
    file('core_alignment_header.embl')

    script:
    """
    mkdir tmp
    export TMPDIR=\$PWD/tmp
    roary -i ${blastp_cutoff} -g ${max_clusters} -e -n -ap -z -p ${num_cores_for_roary} *.gff
    """
}

if (params.tree) {
    process build_tree {
        memory { 15.GB * task.attempt }
        cpus 4
        queue 'long'

        tag {'build tree'}
        publishDir "${output_dir}",
        mode: 'copy'

        input:
        file('core_gene_alignment.complete') from core_gene_alignment

        output:
        file("*.treefile")
        file("*.contree")

        script:
        """
        snp-sites core_gene_alignment.complete -o core_gene_alignment
        iqtree -s core_gene_alignment -m GTR+G -alrt 1000 -bb 1000 -nm 200 -nt AUTO -ntmax 4
        """
    }
}



if (! (workflow.commandLine ==~ /-ansi/)){
  workflow.onComplete {
    Helper.complete_message(params, workflow, version)
  }

  workflow.onError {
    Helper.error_message(workflow)
  }
}

